
import java.util.*;
import java.io.*;
public class hito2 {

	static Scanner leer=new Scanner (System.in);
	public static void main(String[] args) throws IOException{
		int [][] misnumeros=new int [3][5];
		for (int i=0; i<misnumeros.length;i++){
			for (int j=0;j<misnumeros[i].length;j++){
				misnumeros[i][j]=-10;
			}
		}
		int [] numerosganadores=new int [99]; 
		
		
		
		
		System.out.println("Bienvenido");
		
		menu();
		eleccion(misnumeros, numerosganadores);
		
		
			
	}
	public static void menu(){// metodo que imprime por pantalla el menu principal
		System.out.printf("�Que desea hacer?\n"+"1. Poner los numeros aleatorios\n"+"2. Poner los numeros a mano\n"+"3. Jugar\n"+"4. Mostrar el carton\n"+
						  "5. Grabar el carton en un fichero\n"+"6. Recuperar el carton anterior\n"+"0. Fin\n"+"\n");
	}
	
	public static void eleccion (int [][] misnumeros, int [] combinacionganadora)throws IOException{// metodo con un switch para escoger una opcion del menu
		
		int eleccion;
		do{
			System.out.println("Introduzca un numero del menu");
		    eleccion= leer.nextInt();
		}while(eleccion<0 || eleccion>6);
		
		switch (eleccion){
		case 1://coger los numeros aleatoriamente
			misnumerosaleatorios(misnumeros);
			System.out.printf("\nSe ha generado el carton de forma aleatoria\n"+"\n");
			menu();
			eleccion(misnumeros,combinacionganadora);
			break;
			
		case 2://coger los numeros manualmente
			misnumerosmanual(misnumeros);
			System.out.printf("\nSe ha generado el carton de forma manual\n"+"\n");
			menu();
			eleccion(misnumeros,combinacionganadora);
			break;
			
		case 3://simular el juego
			jugar(misnumeros, combinacionganadora);
			menu();
			eleccion(misnumeros,combinacionganadora);
			break;
			
		case 4://muestra el carton
			mostrar (misnumeros);
			menu();
			eleccion(misnumeros,combinacionganadora);
			
			break;
	
		case 5:
			guardar(misnumeros);
			menu();
			eleccion(misnumeros,combinacionganadora);
			
			break;
	
		case 6:
			recuperar(misnumeros);
			menu();
			eleccion(misnumeros,combinacionganadora);
			break;
			
		case 0://finaliza el programa
			System.out.println("Fin del programa");
			break;			
		}
	}
	
	
	public static int[][] misnumerosaleatorios (int [][] numeros){//metodo para hacer el carton aleatoriamente
		for (int i=0; i<numeros.length;i++){
			for (int j=0;j<numeros[i].length;j++){
				numeros[i][j]=-10;
			}
		}
		do{
		for (int i=0; i<numeros.length; i++){//recorriendo filas
			for (int j=0; j<numeros[i].length; j++){//recorriendo columnas
				generaCombinacion(numeros);//se genera la combinacion
			}
		}
		}while (combinacionDecena(numeros));
		
		
		
		return numeros;
	}
	
	public static int [][] generaCombinacion(int [][] combinacion){//metodo que genera el carton
		Random r= new Random();
		int numeroMaximo=99;  // numeroMaximo nos indica el numero m�ximo a generar en la combinaci�n
		int a;
		
		for (int x=0; x<combinacion.length; x++){  // recorremos filas
			for (int j=0; j<combinacion[x].length; j++){//recorremos columnas
				a=r.nextInt(numeroMaximo)+1;
				while(combinacionRepetida(combinacion,a)){
					a=r.nextInt(numeroMaximo)+1;
				}
				while (decenaCarton(combinacion, a)){
					a=r.nextInt(numeroMaximo)+1;
				}
				
				
			combinacion[x][j]=a; // asigna a cada posicion de la matriz un numero aleatorio
			}
		}
	
			
		return combinacion;
	}
	
	public static boolean combinacionDecena (int [][] numero){//metodo que comprueba que los numeros no esten en la misma decena en una linea
		for (int i=0;i<numero.length;i++){
			for (int x=0;x<=numero[i].length-2;x++){//se comprueba que los numeros no esten repetidos
				for (int y=x+1;y<=numero[i].length-1;y++){
					if (((numero[i][x])/10)==((numero[i][y])/10)){
						return true;
					}
				}
			}
			
		}
		return false;
	}
	
	public static boolean combinacionDecenamanual (int [][] numero, int z, int numeroelegido){//metodo que comprueba que los numeros no esten en la misma decena en una linea
					for (int y=0;y<5;y++){
				if ((numeroelegido/10)==((numero[z][y])/10)){
					return true;
				}
			}
	return false;
}
	
	public static boolean decenaCarton (int [][] numeros, int numero ){//metodo que comprueba que en el carton no hay tres numeros en la misma decena
		int contador=0;
		for (int x=0; x<numeros.length; x++){//recorre filas
			for (int y=0; y<numeros[x].length; y++){//recorre columnas
				if (numero/10==(numeros[x][y]/10)){//comprueba si el numero introducido esta en la misma decena que los que ya hay en la matriz
					contador++;//si hay algun numero que este en la misma decena se incrementa el contador
					if (contador>1){
						return true;
					}
				}
				
			}
		}
	
return false;
}
		
		
	
	public static int[][] misnumerosmanual (int [][] numeros){//metodo para escoger la combinacion de forma manual
		
		int numeroelegido;
		for (int x=0; x<numeros.length;x++){
			for (int y=0;y<numeros[x].length;y++){
				numeros[x][y]=-10;
			}
		}
			System.out.println("\nIntroduzca los numeros\n");
				
		for (int i=0; i<numeros.length; i++){//recorriendo filas
			
			for (int j=0; j<numeros[i].length; j++){//recorriendo columnas
				System.out.println("Introduzca el numero de la fila: "+(i+1)+" columna: "+(j+1));
				numeroelegido=leer.nextInt();//escogemos el numero deseado
				
				while (numeroelegido<1 || numeroelegido>99){ //comprueba que cada numero este dentro del rango
					System.out.println("Fuera de rango, escoja un numero entre 1 y 99");
					numeroelegido=leer.nextInt();
					}
				
				
				while (decenaCarton(numeros,numeroelegido)){
					System.out.println("Error, ya hay dos numeros con la misma decena en el carton");
					System.out.println("Introduzca otro numero");
					numeroelegido=leer.nextInt();
				}
				while(combinacionDecenamanual(numeros,i,numeroelegido)){
					System.out.println("Error numeros en la misma decena");
					numeroelegido=leer.nextInt();
				}
				
				while (combinacionRepetida(numeros,numeroelegido)){
					System.out.println("Numero repetido, introduzca otro");
					numeroelegido=leer.nextInt();
					
				}
				
				
				numeros[i][j]=numeroelegido;
				
			}
		
			
				
			
				
				
				
			}
		
			
		
		return numeros;
	}
	
	public static void mostrar (int [][] numeros){//metodo para mostrar todos los numeros del carton
		System.out.printf("\nEl carton es:\n\n");
		for (int x=0; x<numeros.length; x++){//recorriendo filas
			for (int j=0; j<numeros[x].length; j++){//recorriendo columnas
			System.out.print(numeros[x][j]+" ");
			}
			System.out.println();
			System.out.println();
		}
	}
	
	public static boolean combinacionRepetida (int [][] numero, int entero){//metodo que comprueba que los numeros no esten repetidos
		
				for (int x=0; x<3; x++){//recorre filas
					for (int y=0; y<5; y++){//recorre columnas
						if (entero==(numero[x][y])){//comprueba k los numeros no esten en la misma decena en la misma fila
							return true;
						}
					}
				}
			
		return false;
	}
	
	public static void guardar(int [][] numeros)throws IOException{//guarda el carton en el fichero
		FileWriter fichero = null;
        PrintWriter pw = null;
        try
        {
            fichero = new FileWriter("cartonBingo.txt");
            pw = new PrintWriter(fichero);
            
     
            
            pw.println("Numeros:");
            for (int x=0;x<3;x++){//recorre todos mis numeros de la matriz 
            	for (int y=0;y<5;y++){
            	pw.print(numeros[x][y]+" ");
            	
            	}
            	pw.println();
            }    
            
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
           try {
           // Nuevamente aprovechamos el finally para 
           // asegurarnos que se cierra el fichero.
           if (null != fichero)
              fichero.close();
           } catch (Exception e2) {
              e2.printStackTrace();
           }
        }
		
	}
	
	public static int [][] recuperar(int [][] numeros)throws IOException{// permite cargar el carton guardado 
		Scanner lectura=new Scanner(new FileReader("cartonBingo.txt"));
	       lectura.nextLine();
	       for (int i=0;i<numeros.length;i++){
	    	   for (int j=0;j<numeros[i].length;j++){
	    		   numeros[i][j]=lectura.nextInt();
	    		  
	    	   }
	       }
		
		
		return numeros;
	}
	
	public static void jugar (int [][] carton, int[] numeros)throws IOException{//metodo que permite simular una partida de bingo
		Scanner leer=new Scanner(System.in);
		int [][] cartoninicial=new int [3][5];
		
		if  (carton [0][0]<0){
			System.out.println("Error, no existe carton. �Como desea crearlo? automatico=1, manual=2 o recuperar=3");
			int a=leer.nextInt();
			while (a!=1 && a!=2 && a!=3){
				System.out.println("Error, introduzca 1, 2 o 3");
				a=leer.nextInt();
			}
			
		
			switch (a){
			case 1:
				carton=misnumerosaleatorios(carton);
				cartoninicial=igualarCartones(carton,cartoninicial);
				System.out.printf("\nSe ha generado el carton de forma aleatoria\n"+"\n");
				break;
				
			case 2:
				carton=misnumerosmanual(carton);
				cartoninicial=igualarCartones(carton,cartoninicial);
				System.out.printf("\nSe ha generado el carton de forma manual\n"+"\n");
				break;
				
			case 3:
				carton=recuperar(carton);
				cartoninicial=igualarCartones(carton,cartoninicial);
				break;
			}	
		}else{
		cartoninicial=igualarCartones(carton,cartoninicial);
		}
		numeros=generaCombinacionganadora(numeros);	
		carton=compararBingo(carton,numeros,cartoninicial);
		System.out.println("���BINGO!!!\n");	
		
	}
	
	public static int [] generaCombinacionganadora(int [] combinacion){//metodo para generar el bombo
		Random r= new Random();
		int numeroMaximo=99;  // numeroMaximo nos indica el numero m�ximo a generar en la combinaci�n
		int a;
		for (int x=0; x<combinacion.length; x++){  // recorremos desde 0 hasta la longitud del array de entrada
			a= r.nextInt(numeroMaximo)+1;
			while(bomboRepetido(combinacion,a)){
				a=r.nextInt(numeroMaximo)+1;
			}
			combinacion[x]=a;
			
		}		return combinacion;
	}
	
	public static int [][] compararBingo (int [][] carton, int [] bombo, int [][] cartoninicial)throws IOException{//metodo que compara el carton jugado con el bombo para ver si se hace bingo
		int i=0;
		boolean [][]a=new boolean[3][5];
		boolean t=true;
		for (int b =0; b<a.length;b++){
			for (int c=0; c<a[b].length;c++){
			a[b][c]=true;
			}
		}
		
		while (i<bombo.length && cartonNegativo(carton,cartoninicial)){
			
			System.out.println("\nLa bola n� "+(i+1)+" es:"+bombo[i]+"\n");
			System.out.println("Faltan las bolas:");
			
				for (int x=i+1;x<bombo.length;x++){
				System.out.print(" "+bombo[x]);
				}
			
			System.out.println();
			
				for (int j=0;j<carton.length;j++){
					for (int k=0;k<carton[j].length;k++){
						if(carton[j][k]==bombo[i]){
						carton[j][k]=carton[j][k]*-1;	
						}
					
					}
				}
				
			a=cantarLinea(carton,cartoninicial);
			
			
			if (t){
			if (((a[0][0]==false && a[0][1]==false )&& (a[0][2]==false && a[0][3]==false)) && a[0][4]==false){
				System.out.println("\n���LINEA!!!");
				t=false;
			}else if (((a[1][0]==false && a[1][1]==false )&& (a[1][2]==false && a[1][3]==false)) && a[1][4]==false){
				System.out.println("\n���LINEA!!!");
				t=false;
			}else if (((a[2][0]==false && a[2][1]==false )&& (a[2][2]==false && a[2][3]==false)) && a[2][4]==false){
				System.out.println("\n���LINEA!!!");
				t=false;
			}
			
		}
	
				
						
			mostrar(carton);
			i++;
		}
		return carton;
	}
	
	public static boolean bomboRepetido (int [] numero, int entero){//metodo que comprueba que los numeros no esten repetidos
		
		
			for (int y=0; y<numero.length; y++){//recorre columnas
				if (entero==(numero[y])){//comprueba k los numeros no esten en la misma decena en la misma fila
					return true;
				}
			}	
return false;
}
	
	public static boolean  cartonNegativo(int [][]carton, int [][] cartoninicial){//metodo que compara el carton inicial con el carton jugado para manejar el bucle while del metodo jugar
		boolean [][] verdad=new boolean [3][5];
		boolean x;
		boolean y=true;
		for (int j=0;j<carton.length;j++){
			for (int k=0;k<carton[j].length;k++){
				if (carton[j][k]*(-1)!=(cartoninicial[j][k])){
						x=true;
				
				}else{
					x=false;
				}
				verdad[j][k]=x;
				
				if (verdad[j][k]==y){
					return true;
				}
			}	
		}
		return false;
	}
	public static int [][] igualarCartones (int[][] carton,int [][] cartoninicial){//metodo que iguala el carton jugado con el carton inicial
		
		for (int i=0;i<carton.length;i++){
			for (int j=0;j<carton[i].length;j++){
				cartoninicial[i][j]=carton[i][j];
				
			}
		
		}
		return cartoninicial;	
	}
	
	public static boolean [][] cantarLinea(int [][] carton, int[][] cartoninicial){//metodo que compara las lineas para saber si se ha hecho linea
		boolean x;
		boolean [][]verdad=new boolean [3][5];
		for (int i=0;i<verdad.length;i++){
			for (int j=0;j<verdad[i].length;j++){
		
			if (carton[i][j]*(-1)==cartoninicial[i][j]){
				x=false;
			}else{
				x=true;
			}
			verdad [i][j]=x;
			}
		}
		return verdad;
	}
}